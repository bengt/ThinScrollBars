ThinScrollBars
==============

Thin scrollbars for webkit browsers with a userscript. Basically an Greasemonkey version of [the ThinScrollBar Chrome extension](https://chrome.google.com/webstore/detail/ojmmnceaidnmminjjffpndcbdibelgam).

You can [give it a try](http://bigben87.github.com/ThinScrollBars/) without installing on GitHub Pages.

Installation on Google Chrome
-----------------------------

-   Install [Tampermonkey](https://chrome.google.com/webstore/detail/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=de).
-   Install [ThinScrollBars](https://github.com/bigben87/ThinScrollBars/raw/master/ThinScrollBars.user.js) and answer "OK" to the dialogue.


